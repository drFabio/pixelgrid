import org.redi.school.grid.PixelFrame;

public class Main {
    public static void main(String[] args) {
        PixelFrame frame = new PixelFrame();
        for (int i =0; i < 10; i ++) {

            // H
            frame.addColumn(0);
            frame.addLine(3);
            frame.addColumn(6);
            frame.paint();

            // E
            frame.addLine(0);
            frame.addColumn(0);
            frame.addLine(3);
            frame.addLine(6);
            frame.paint();

            // L
            frame.addColumn(0);
            frame.addLine(6);
            frame.paint();


            // L
            frame.addColumn(0);
            frame.addLine(6);
            frame.paint();

            // 0
            frame.addLine(0);
            frame.addLine(6);
            frame.addColumn(0);
            frame.addColumn(6);
            frame.paint();

            // Blank
            frame.paint();
        }

    }
}
