package org.redi.school.grid;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;


public class PixelFrame {
    private static int stroke = 3;

    private static int numOfLines = 7;
    private static int width = numOfLines * (100 + stroke);
    private static int height = numOfLines * (100 + stroke);
    private boolean isFirstPaint = true;
    protected ArrayList<Pixel> pixels = new ArrayList();
    protected ArrayList<Integer> lines = new ArrayList();
    protected ArrayList<Integer> columns = new ArrayList();
    private static int paintWaitTime = 700;
    private static int refreshWaitTime = 200;

    private GridPanel panel;

    public PixelFrame() {
        JFrame grid = new JFrame();
        grid.setResizable(false);
        panel = new GridPanel(width, height, numOfLines, stroke);
        grid.getContentPane().add(panel);
        grid.setSize(width, height);
        grid.setVisible(true);
        Dimension actualSize = grid.getContentPane().getSize();
        int extraHeight = height - actualSize.height;
        grid.setSize(width, height + extraHeight);
    }

    public void addPixel(int x, int y) {
        this.pixels.add(new Pixel(x, y));
    }

    public void addLine(int position) {
        this.lines.add(position);
    }

    public void addColumn(int position) {
        this.columns.add(position);
    }

    public void paint() {

        try {
            if (isFirstPaint) {
                isFirstPaint = false;
            } else {
                Thread.sleep(paintWaitTime);

            }
            panel.clearAll();
            panel.reRender();
            Thread.sleep(refreshWaitTime);
            panel.setColumns(columns);
            panel.setLines(lines);
            panel.setPixels(pixels);
            panel.reRender();
            pixels = new ArrayList();
            lines = new ArrayList();
            columns = new ArrayList();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
