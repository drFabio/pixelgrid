package org.redi.school.grid;

public class Pixel {
    public final int x;
    public final int y;

    protected Pixel(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public String toString() {
        return String.format("%d, %d", x, y);
    }
}