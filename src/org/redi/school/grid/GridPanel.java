package org.redi.school.grid;

import org.redi.school.grid.Pixel;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Line2D;
import java.util.ArrayList;

public class GridPanel extends JPanel {
    protected int width;
    protected int height;
    protected int numOfCells;
    protected int stroke;

    protected ArrayList<Pixel> pixels = new ArrayList();
    protected ArrayList<Integer> lines = new ArrayList();
    protected ArrayList<Integer> columns = new ArrayList();

    public GridPanel(int width, int height, int numOfCells, int stroke) {
        this.width = width;
        this.height = height;
        this.numOfCells = numOfCells;
        this.stroke = stroke;
    }

    public void reRender() {
        repaint();
        revalidate();
    }

    public void setPixels(ArrayList<Pixel> pixels) {
        this.pixels = pixels;
    }

    public void setLines(ArrayList<Integer> lines) {
        this.lines = lines;
    }

    public void setColumns(ArrayList<Integer> columns) {
        this.columns = columns;
    }
    public void clearAll() {
        this.pixels = new ArrayList();
        this.lines = new ArrayList();
        this.columns = new ArrayList();
    }
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.DARK_GRAY);
        for (Pixel pixel : pixels) {
            drawPixel(g2, pixel.x, pixel.y);
        }
        for (Integer line : lines) {
            drawLine(g2, line);
        }
        for (Integer column : columns) {
            drawColumn(g2, column);
        }
        drawGrid(g2);
    }

    protected void drawGrid(Graphics2D g) {
        g.setColor(Color.BLACK);
        drawHorizontal(g);
        drawVertical(g);
    }

    public void drawHorizontal(Graphics2D g2) {
        float y = 0;
        float spacing = width / numOfCells;
        for (int i = 0; i < numOfCells; i++) {
            Line2D lin = new Line2D.Float(0, y, width, y);
            g2.setStroke(new BasicStroke(stroke));
            g2.draw(lin);
            y += spacing;
        }
    }

    public void drawVertical(Graphics2D g2) {
        float x = 0;
        float spacing = height / numOfCells;
        for (int i = 0; i < numOfCells; i++) {
            Line2D lin = new Line2D.Float(x, 0, x, height);
            g2.setStroke(new BasicStroke(stroke));
            g2.draw(lin);
            x += spacing;
        }
    }

    protected void drawPixel(Graphics2D g, int line, int cell) {
        int pixelWidth = width / numOfCells;
        int pixelHeight = height / numOfCells;
        int x = line * pixelWidth;
        int y = cell * pixelHeight;
        g.fillRect(x, y, pixelWidth, pixelHeight);
    }

    protected void drawColumn(Graphics2D g, int line) {
        for (int i = 0; i < numOfCells; i++) {
            drawPixel(g, line, i);
        }
    }

    protected void drawLine(Graphics2D g, int column) {
        for (int i = 0; i < numOfCells; i++) {
            drawPixel(g, i, column);
        }
    }
}
